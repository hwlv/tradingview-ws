# -*- coding: utf-8 -*-
import time

from flask import Flask, request
from flask import jsonify
import json
import datetime
import sys
from pymongo import MongoClient
import calendar
import re
# from flask_cors import *
# from flask_socketio import SocketIO

from flask_cors import CORS

from handle import DateEncoder
from util import getResolutions, totimestamp

app = Flask(__name__)
CORS(app, supports_credentials=True)

collections = []

reload(sys)
sys.setdefaultencoding('utf8')

client = MongoClient('mongodb://144.202.119.59:27017')
db = client.Flysword_1Min_Db
db1History = client.Flysword_1Min_Db
dbPosition= client.Flysword_Position_Db
collection = db['BTC/USDT.HUOBI']

'''
#设置这一选项为True如果您的datafeed只提供所有商品集合的完整信息，并且无法进行商品搜索或单个商品解析。
// supports_search和supports_group_request两者之中有只有一个可以为True。
// supported_resolutions
一个表示服务器支持的分辨率数组，分辨率可以是数字或字符串。 如果分辨率是一个数字，它被视为分钟数。
// 字符串可以是“*D”，“*W”，“_M”（_的意思是任何数字）
'''


@app.route('/config')
def config():
    map = {}
    map["supports_search"] = True  # 设置这一选项为True如果你的datafed支持商品查询和人商品解析逻辑。
    map["supports_group_request"] = False
    map["supported_resolutions"] = getResolutions()
    map["supports_marks"] = False  # 是否支持在K线上显示标记
    map["supports_timescale_marks"] = False  # 布尔值来标识您的是否支持时间刻度标记。
    map["supports_time"] = True  # 将此设置为True假如您的datafeed提供服务器时间（unix时间）。 它用于调整时间刻度上的价格比例。
    exchanges = []
    exarr1 = {}
    exarr1['value'] = ''
    exarr1['name'] = 'All Exchanges'
    exarr1['desc'] = 'All Exchanges'
    exarr2 = {}
    exarr2['value'] = ''
    exarr2['name'] = 'All Exchanges'
    exarr2['desc'] = 'All Exchanges'
    exarr3 = {}
    exarr3['value'] = ''
    exarr3['name'] = 'All Exchanges'
    exarr3['desc'] = 'All Exchanges'
    exchanges.append(exarr1)
    exchanges.append(exarr2)
    exchanges.append(exarr3)

    symbols_types = []
    typeObj1 = {}
    typeObj1['name'] = 'All types'
    typeObj1['value'] = ''
    typeObj2 = {}
    typeObj2['name'] = 'Stock'
    typeObj2['value'] = 'Stock'
    typeObj3 = {}
    typeObj3['name'] = 'Index'
    typeObj3['value'] = 'Index'
    symbols_types.append(typeObj1)
    symbols_types.append(typeObj2)
    symbols_types.append(typeObj3)
    map['exchanges'] = exchanges
    map['symbols_types'] = symbols_types
    map['supported_resolutions'] = getResolutions()
    return json.dumps(map)


@app.route('/collections')
def getCollections():
    collections = db1History.collection_names()
    collections2 = dbPosition.collection_names()
    return json.dumps(collections+collections2)


@app.route('/symbols', methods=['POST', 'GET'])
def symbols():
    testTrade = 'btc-usd'
    testTrade = request.args.get('symbol')
    print testTrade
    symbolInfo = {}
    symbolInfo["name"] = testTrade
    symbolInfo["exchange-traded"] = testTrade
    symbolInfo["exchange-listed"] = "testTrade"  # 交易所名称
    symbolInfo["timezone"] = "UTC"  # 这个商品的交易所时区
    symbolInfo["minmov"] = 1  # 价格最小波动
    symbolInfo["minmov2"] = 0
    symbolInfo["pointvalue"] = 1
    symbolInfo["session"] = "24x7"  # 商品交易时间
    symbolInfo["has_intraday"] = True  # 是否支持日内分钟历史数据
    symbolInfo["has_weekly_and_monthly"] = True  # 是否支持日周 月
    # symbolInfo["has_no_volume"= False);#布尔表示商品是否拥有成交量数据。
    symbolInfo["description"] = testTrade  # 商品简介
    symbolInfo["type"] = "stock"  # 图形仪表的类型
    symbolInfo["supported_resolutions"] = getResolutions()
    symbolInfo["pricescale"] = 100000  # 价格精度
    symbolInfo["ticker"] = testTrade  # 它是您的商品体系中此商品的唯一标识符
    return json.dumps(symbolInfo)


@app.route('/time')
def tradeTime():
    timeNow = int(time.time())
    print timeNow
    return str(timeNow)


@app.route('/api')
def api():
    result = {
        'status': 'success',
        'data': 'Hello, world!',
    }
    return jsonify(result)


def date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError


def getTimeBy():
    return ''


@app.route('/history')
def history():
    symbol = request.args.get('symbol')
    fromStr = request.args.get('from')
    to = request.args.get('to')
    resolution = request.args.get('resolution')
    isStrategy=False
    if('Strategy' in symbol):
        isStrategy=True
        collection = dbPosition[symbol]
    else:
        collection = db[symbol]

    # start = datetime.datetime.fromtimestamp(int(fromStr))  # 1522742760
    # end = datetime.datetime.fromtimestamp(int(to))  # 1522788000
    start = datetime.datetime.utcfromtimestamp(int(fromStr))  # 1522742760
    start2 = datetime.datetime.utcfromtimestamp(int(fromStr))  # 1522742760
    end = datetime.datetime.utcfromtimestamp(int(to))  # 1522788000
    # if resolution=='1':
    print (start, end)
    results = collection.find({"datetime": {"$gte": start, "$lt": end}})
    # if resolution == '1D':
    resCount = results.count()
    responseDic = {}
    close = []
    low = []
    high = []
    open = []
    t = []
    volume = []
    if resCount == 0:
        print 'nodata'
        return json.dumps({'s': 'no_data'})
    else:


        for doc in results:
            for k, v in doc.items():
                if k == 'datetime':
                    print '********************1'
                    print type(k)
                    print type(v)
                    if isinstance(v, datetime.datetime):
                        # v = v.strftime('%Y-%m-%d %H:%M:%S')
                        # v = time.mktime(v.timetuple())
                        # v=calendar.timegm(v.utctimetuple())
                        # v = v + datetime.timedelta(hours=-8)  # 中国默认时区
                        # timestamp = total_seconds(dt - EPOCH)
                        # return long(timestamp)
                        v=totimestamp(v)
                        t.append(long(v))
                    print v
                if k == '_id':
                    continue
                if k == 'close':
                    close.append(v)
                if k == 'open':
                    open.append(v)
                if k == 'low':
                    low.append(v)
                if k == 'high':
                    high.append(v)
                if k == 'volume':
                    volume.append(v)
                #策略数据
                if k == 'pos':
                    high.append(0)
                    open.append(0)
                    low.append(0)
                    close.append(0)
                if k == 'TotalProfit':
                    volume.append(v)

                responseDic['c'] = close
                responseDic['o'] = open
                responseDic['v'] = volume
                responseDic['h'] = high
                responseDic['l'] = low
                responseDic['t'] = t
                responseDic['s'] = 'ok'
    print responseDic
    return json.dumps(responseDic)


@app.route('/api/getOne')
def getOne():
    db = client.Flysword_1Min_Db
    collection = db['BTC/USD.YOBIT']
    oneColl = collection.find_one({'data': '1'})
    print oneColl.get('high')
    print oneColl.get('datetime')
    responseDic = {}
    for k, v in oneColl.items():
        if k == 'datetime':
            print '********************1'
            print type(k)
            print type(v)
            if isinstance(v, datetime.datetime):
                # v = v.strftime('%Y-%m-%d %H:%M:%S')
                v = time.mktime(v.timetuple())
            print v
        if k == '_id':
            continue
        responseDic[k] = v
        print k, v

    print responseDic
    return json.dumps(responseDic)


if __name__ == '__main__':
    # app.run()
    app.debug = True
    app.run()
